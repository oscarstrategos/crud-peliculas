<?php
class ConsumoMicroServicios {

    const URL_TOKEN = "http://localhost:20027/authtoken";
    const URL_TOKENV = "http://loginless:20027/authtoken";
    const URL_PROXY = "http://localhost:3002/api/v1/game";
    const TYPE = 'PRUEBA'; 

	static function obtenerCredencial($categoria){
        
        $idToken = self::obtenerTokenLess();

		$postRequest = array(
			'category' 		=> 'OBTENERCREDENCIAL',
			'name' 			=> 'REMEDIACIONES',
			'type' 			=> 'OFA',
			'categoria' 	=> $categoria.'|'.self::TYPE,
			'rutaValidacion' => self::URL_TOKENV.'/validateToken'
		);

		$datosLogeo = self::consumirProxy($idToken, $postRequest);

		return $datosLogeo;
	}

	static function obtenerCredenciales($categorias){
        
        $idToken = self::obtenerTokenLess();

		$postRequest = array(
			'aplicacion' 	=>  self::TYPE,
			'category' 		=> 'OBTENERCREDENCIALES',
			'name' 			=> 'REMEDIACIONES',
			'type' 			=> 'OFA',
			'categorias' 	=> $categorias,
			'rutaValidacion' => self::URL_TOKENV.'/validateToken'
		);

		$datosLogeo = self::consumirProxy($idToken, $postRequest);

		return $datosLogeo;
	}

	static function consumirProxy($idToken, $postRequest) {
		$getRequest =  array(
		'Content-Type: application/json; charset=utf-8',
		'Authorization: Bearer '.$idToken
		);

	
		$cURLConnection = curl_init(self::URL_PROXY);
		curl_setopt($cURLConnection, CURLOPT_POST, 1);
		curl_setopt($cURLConnection, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($cURLConnection, CURLOPT_POSTFIELDS, json_encode($postRequest));
		curl_setopt($cURLConnection, CURLOPT_HTTPHEADER, $getRequest);
		curl_setopt($cURLConnection, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
		curl_setopt($cURLConnection, CURLINFO_HEADER_OUT, true);
		curl_setopt($cURLConnection, CURLOPT_VERBOSE, true);
		curl_setopt($cURLConnection, CURLOPT_TIMEOUT, 60);
		curl_setopt($cURLConnection, CURLOPT_PROXY,'');
		curl_setopt($cURLConnection, CURLOPT_RETURNTRANSFER, true);
	
		$apiResponse = json_decode(curl_exec($cURLConnection), true);
	
		return $apiResponse;
	}

	static function obtenerTokenLess() {
		$rutaToken = self::URL_TOKEN.'/getToken';
		$postRequest = array(
			'sitibundus' 	=> self::TYPE,
			'undivago' 	=> $_SERVER['SERVER_ADDR'],
			'usertype'	=> 1
		);
	
		
		$cURLConnection = curl_init( $rutaToken );
		curl_setopt($cURLConnection, CURLOPT_POST, 1);
		curl_setopt($cURLConnection, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($cURLConnection, CURLOPT_POSTFIELDS, json_encode($postRequest));
		curl_setopt($cURLConnection, CURLOPT_HTTPHEADER, array('Content-Type:application/json; charset=utf-8'));
		curl_setopt($cURLConnection, CURLOPT_TIMEOUT, 60);
		curl_setopt($cURLConnection, CURLOPT_PROXY,'');
		curl_setopt($cURLConnection, CURLOPT_RETURNTRANSFER, true);
		
		$apiResponse = json_decode(curl_exec($cURLConnection)); 

		curl_close($cURLConnection);

		return ( $apiResponse->id_token === "" || !$apiResponse ) ? "" : $apiResponse->id_token;
	}

	static function obtenerToken($sitibundus,$undivago) {
		$rutaToken = self::URL_TOKEN.'/getToken';
		$postRequest = array(
			'sitibundus' 	=> $sitibundus,
			'undivago' 	=> $undivago,
			'usertype'	=> 1
		);
	
		
		$cURLConnection = curl_init( $rutaToken );
		curl_setopt($cURLConnection, CURLOPT_POST, 1);
		curl_setopt($cURLConnection, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($cURLConnection, CURLOPT_POSTFIELDS, json_encode($postRequest));
		curl_setopt($cURLConnection, CURLOPT_HTTPHEADER, array('Content-Type:application/json; charset=utf-8'));
		curl_setopt($cURLConnection, CURLOPT_TIMEOUT, 60);
		curl_setopt($cURLConnection, CURLOPT_PROXY,'');
		curl_setopt($cURLConnection, CURLOPT_RETURNTRANSFER, true);
		
		$apiResponse = json_decode(curl_exec($cURLConnection)); 

		curl_close($cURLConnection);

		return ( $apiResponse->id_token === "" || !$apiResponse ) ? "" : $apiResponse->id_token;
	}


}


?>