<?php
require_once('cnx_user.inc');   

$host       = $DB_SERVER;
$dbname     = $DB;
$user       = $DB_LOGIN;
$password   = $DB_PASS;
$port       = $PORT;

$cnx = pg_connect("host=$host port=$port dbname=$dbname user=$user password=$password");

$nombre = $_POST['nombre'];

try
{
	pg_query($cnx,"BEGIN;");
	
	$peliculas = pg_query($cnx,"select idPelicula, nombre from Pelicula where nombre like '%$nombre%';");
    				
	while($row = pg_fetch_array($peliculas))
	{			
        $str = str_replace($nombre, "", $row['nombre']);
        $num = $row['idPelicula'];
		pg_query($cnx,"update Pelicula set nombre='$str' where idPelicula=$num");			
	}
	
    pg_query($cnx,"COMMIT;");
	echo 'Palabra clave eliminada...';		
}
catch(Exception $e)
{
	$this->sql_exh('ROLLBACK;');
	echo 'Error...<a href="index.php">Regresar</a>';
}

echo '<a href="index.php">Regresar</a>';