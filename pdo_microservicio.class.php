<?php
/**
* MicroService Database class
*
* @author:    	Joel Silva Lopez
* @version: 	TL-0.2
*/

class MicroService{

	private $rutaTokenLess = 'http://10.49.123.33:20027/authtoken';
    private $rutaProxy = 'http://10.49.123.33:3002/api/v1/game';
    private $rutaValidate = 'http://localhost:20027/authtoken';
	private $type = 'PROV_CI';

	private $connection;

	private $err_msg = "";

	public function __construct($connection){
		$this->connection = strtolower($connection);
	}

	//Ejecuta un query simple
	public function query($sql_statement){
		$this->err_msg = "";
		try{
			$token = $this->obtenerTokenLess();
			$res = $this->consumirProxy($token, $sql_statement);
			return $res;
		}catch(Exception $e){
			$this->err_msg = "Error: ". $e->getMessage();
			return false;
		}
	}

	//Obtiene la primera celda de la primera columna
	public function query_single($sql_statement){
		$this->err_msg = "";
		try{
			$token = $this->obtenerTokenLess();
			$res = $this->consumirProxy($token, $sql_statement);
			if (isset($res[0]))
			{
				$res = array_values($res[0]);
				return htmlspecialchars($res[0]);
			}
			return null;
		}catch(Exception $e){
			$this->err_msg = "Error: ". $e->getMessage();
			return false;
		}
	}

	//Llamando un query especial (que tiene su propio category)
	// ['TRAN_EMMX', []]
	public function execute($sql_statement){
		$this->err_msg = "";
		try{
			$token = $this->obtenerTokenLess();
			$res = $this->otherProxy($token, $sql_statement);
			return $res;
		}catch(Exception $e){
			$this->err_msg = "Error: ". $e->getMessage();
			return false;
		}
	}

	//Get the latest error ocurred in the connection
	public function getError(){
		return trim($this->err_msg) != "" ? $this->err_msg : "";
	}

	private function obtenerTokenLess() {
		$rutaToken = $this->rutaTokenLess.'/getToken';
		$postRequest = array(
			'sitibundus' 	=> $this->type,
			'undivago' 	=> $_SERVER['SERVER_ADDR'],
			'usertype'	=> 1
		);
		
		$cURLConnection = curl_init( $rutaToken );
		curl_setopt($cURLConnection, CURLOPT_POST, 1);
		curl_setopt($cURLConnection, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($cURLConnection, CURLOPT_POSTFIELDS, json_encode($postRequest));
		curl_setopt($cURLConnection, CURLOPT_HTTPHEADER, array('Content-Type:application/json; charset=utf-8'));
		curl_setopt($cURLConnection, CURLOPT_TIMEOUT, 60);
		curl_setopt($cURLConnection, CURLOPT_PROXY,'');
		curl_setopt($cURLConnection, CURLOPT_RETURNTRANSFER, true);
		
		$apiResponse = json_decode(curl_exec($cURLConnection));
		$status_code = curl_getinfo($cURLConnection, CURLINFO_HTTP_CODE);
		
		if ($status_code != 200) {
			throw new Exception("Error al consumir token");
		}
		
		curl_close($cURLConnection);

		return ( $apiResponse->id_token === "" || !$apiResponse ) ? "" : $apiResponse->id_token;
	}

	//Necesita el token, una conexion y un sql-statement con la siguiente estructura:
	// $cSql = ['<query>',[param1,param2,...,paramN]];
	private function consumirProxy($idToken, $cSql) {

		$params = [];
		if (isset($cSql[1])){
			$params = $cSql[1];
		}

		$postRequest = array(
			'category' => 'EXEC_QUERY',
			'name' => 'REMEDIACIONES',
			'type' => $this->type,
			'rutaValidacion' => $this->rutaValidate.'/validateToken',
			'connection' => $this->connection,
			'fun' => $cSql[0],
        	'params' => $params,
		);

		$getRequest =  array(
			'Content-Type: application/json; charset=utf-8',
			'Authorization: Bearer '.$idToken
		);

		$cURLConnection = curl_init($this->rutaProxy);
		curl_setopt($cURLConnection, CURLOPT_POST, 1);
		curl_setopt($cURLConnection, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($cURLConnection, CURLOPT_POSTFIELDS, json_encode($postRequest));
		curl_setopt($cURLConnection, CURLOPT_HTTPHEADER, $getRequest);
		curl_setopt($cURLConnection, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
		curl_setopt($cURLConnection, CURLINFO_HEADER_OUT, true);
		curl_setopt($cURLConnection, CURLOPT_VERBOSE, true);
		curl_setopt($cURLConnection, CURLOPT_TIMEOUT, 60);
		curl_setopt($cURLConnection, CURLOPT_PROXY,'');
		curl_setopt($cURLConnection, CURLOPT_RETURNTRANSFER, true);
	
		$apiResponse = json_decode(curl_exec($cURLConnection),true);

		$status_code = curl_getinfo($cURLConnection, CURLINFO_HTTP_CODE);
		
		if ($status_code != 200 && $status_code != 204) {
			throw new Exception("Error en llamada a proxy");
		}

		curl_close($cURLConnection);
	
		return $apiResponse;
	}

	//Necesita el token y un sql-statement con la siguiente estructura:
	// $cSql = ['<category>',[param1,param2,...,paramN]];
	private function otherProxy($idToken, $cSql) {

		$params = [];
		if (isset($cSql[1])){
			$params = $cSql[1];
		}

		$postRequest = array(
			'category' => $cSql[0],
			'name' => 'REMEDIACIONES',
			'type' => $this->type,
			'rutaValidacion' => $this->rutaValidate.'/validateToken',
        	'params' => $params,
		);

		$getRequest =  array(
			'Content-Type: application/json; charset=utf-8',
			'Authorization: Bearer '.$idToken
		);

		$cURLConnection = curl_init($this->rutaProxy);
		curl_setopt($cURLConnection, CURLOPT_POST, 1);
		curl_setopt($cURLConnection, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($cURLConnection, CURLOPT_POSTFIELDS, json_encode($postRequest));
		curl_setopt($cURLConnection, CURLOPT_HTTPHEADER, $getRequest);
		curl_setopt($cURLConnection, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
		curl_setopt($cURLConnection, CURLINFO_HEADER_OUT, true);
		curl_setopt($cURLConnection, CURLOPT_VERBOSE, true);
		curl_setopt($cURLConnection, CURLOPT_TIMEOUT, 60);
		curl_setopt($cURLConnection, CURLOPT_PROXY,'');
		curl_setopt($cURLConnection, CURLOPT_RETURNTRANSFER, true);
	
		$apiResponse = json_decode(curl_exec($cURLConnection),true);

		$status_code = curl_getinfo($cURLConnection, CURLINFO_HTTP_CODE);
		
		if ($status_code != 200 && $status_code != 204) {
			throw new Exception("Error en llamada a proxy");
		}

		curl_close($cURLConnection);
	
		return $apiResponse;
	}

}
?>
